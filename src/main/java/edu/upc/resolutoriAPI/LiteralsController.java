/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.resolutoriAPI;

import edu.upc.mpi.BaseDeDades.Connector;
import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.chase.Chase;
import edu.upc.mpi.chase.NodeArbre;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschemaRGD.EGD;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import edu.upc.mpi.logicschemaRGD.RGD;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
/**
 *
 * @author guille
 */
@RestController
public class LiteralsController {
    private LogicSchemaRGD esquema = new LogicSchemaRGD();
    private String resposta;
    
    @GetMapping("/esquema")
    public Pair<List<OrdinaryLiteral>, List<String>> obteLiterals(@RequestParam(value="nomBD") String nomBD,@RequestParam(value="URL") String url,@RequestParam(value="username") String usuari, @RequestParam(value="clau") String clau) throws Exception {
        Connector con = new Connector(url, usuari, clau);
        List<OrdinaryLiteral> ordliterals = con.obtenirTuplesBD(nomBD);
        List<String> literalsText = new ArrayList<>();
        for(OrdinaryLiteral or : ordliterals) {
            literalsText.add(or.toString());
        }
        
        Pair<List<OrdinaryLiteral>, List<String>> pair = Pair.of(ordliterals,literalsText);
        return pair;
    }
    
    @GetMapping("/bd")
    public String comprovaConnexio(@RequestParam(value="nomBD") String nomBD,@RequestParam(value="URL") String url,@RequestParam(value="username") String usuari, @RequestParam(value="clau") String clau) throws Exception {
        Connector con = new Connector(url, usuari, clau);
        Connection conn = con.getConnection();
        if(conn == null) resposta = "Connexió incorrecta";
        else resposta = "Connexió correcta";
        return resposta;
    }
    
    @GetMapping("/esquema/taula")
    public List<OrdinaryLiteral> obteLiteralsTaula(@RequestParam(value="nomBD") String nomBD,@RequestParam(value="URL") String url,@RequestParam(value="username") String usuari, @RequestParam(value="clau") String clau, @RequestParam(value="nomtaula") String taula) throws Exception {
        Connector con = new Connector(url, usuari, clau);
        Connection cn = con.getConnection();
        List<OrdinaryLiteral> ordliterals = Connector.obtenirTuplesTaula(taula, cn);
        return ordliterals;
    }
    
    @PostMapping("/esquema/chase")
    public Pair<ArbreReparacions, List<List<String>>>  executaEsdeveniments(@RequestBody Map<String, Object> regles){
        List<String> Literals = new LinkedList<>();
        List<String> NousLiterals = new LinkedList<>();
        List<String> reglesNoves = new LinkedList<>();
        for (Map.Entry<String,Object> entry : regles.entrySet()) {
            if(entry.getKey().equals("literals")) {
                Literals = (List<String>) entry.getValue();
            }
            else if(entry.getKey().equals("nousLiterals")){
                NousLiterals = (List<String>)entry.getValue();
            }
            else if(entry.getKey().equals("regles")) {
                reglesNoves = (List<String>) entry.getValue();
            }
        }
        String esquema = "";
        for(String rN : reglesNoves) {
           esquema += rN + "\n";
        }
        for(String rN : Literals) {
           esquema += rN + "\n";
        }
        for(String rN : NousLiterals) {
           esquema += rN + "\n";
        }
        System.out.println(esquema);
        
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();   
        List<RGD> regles_rgd = resultLogicSchemaRGD.getAllRGDs();
        List<EGD> regles_egd = resultLogicSchemaRGD.getAllEGDs();
        List<NormalClause> regles_esquema = new LinkedList<>();
        regles_esquema.addAll(regles_rgd);
        regles_esquema.addAll(regles_egd);
        List<OrdinaryLiteral> fetsEntrada = resultLogicSchemaRGD.getAllFetsEntrada();
        List<String> regles1 = new LinkedList<>();
//        for(NormalClause r : regles_esquema) {
//            regles1.add(r.toString());
//        }
//        for(OrdinaryLiteral fE : fetsEntrada) {
//            System.out.println(fE.toString());
//        }
//        List<OrdinaryLiteral> fetsTotals =  new LinkedList<>();
        List<OrdinaryLiteral> fetsReparacio =  new LinkedList<>();
        Chase ch = new Chase();
        NodeArbre inici = new NodeArbre(new LinkedList<>());
        ArbreReparacions ar = new ArbreReparacions(new ArrayList<>());
        List<String> reparacions = new LinkedList<>();
        List<List<String>> llistes = new LinkedList<>();
        if(resultLogicSchemaRGD.teDisjuncions()) {
            ch.chaseDisjuncionsRecursiu(inici, regles_esquema, fetsEntrada, 0);
            //reparacions.add(NodeArbre.serialize(inici));
            carregaArbre(inici,ar);
            omplirLlistes(ar,llistes);
            Pair<ArbreReparacions, List<List<String>>> pair = Pair.of(ar,llistes);
            return pair;
            //return ar;
        }
        else {
            fetsReparacio = ch.chase(regles_esquema, fetsEntrada);
            ar.setValor(fetsReparacio);
            llistes.add(new LinkedList<>(ar.obteValor()));
            Pair<ArbreReparacions, List<List<String>>> pair = Pair.of(ar,llistes);
            return pair;
        }
    }
    
    public void carregaArbre(NodeArbre node, ArbreReparacions arrel) {
        if(node.esFulla() && node.obteValor() != null) {
            arrel.afegirFill(node.obteValor());
        }
        else {
            for(NodeArbre fill : node.obteFills()) {
                carregaArbre(fill,arrel);
                if(node.obteValor() != null){
                    arrel.setValor(node.obteValor());
                }
            }
        }
    }
    
    public void omplirLlistes(ArbreReparacions arrel, List<List<String>> llistes) {
        if(arrel.esFulla()) {
            llistes.add(new LinkedList<>(arrel.obteValor()));
        }
        else {
            for(ArbreReparacions fill : arrel.getChildren()){
                omplirLlistes(fill,llistes);
                for(List<String> l : llistes){
                    l.addAll(arrel.obteValor());
                }
            }
        }
    }
    
}
