package edu.upc.resolutoriAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ResolutoriApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResolutoriApiApplication.class, args);
	}
        @Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/bd").allowedOrigins("http://localhost:3000");
                                registry.addMapping("/esquema").allowedOrigins("http://localhost:3000");
                                registry.addMapping("/esquema/taula").allowedOrigins("http://localhost:3000");
                                registry.addMapping("/esquema/regles").allowedOrigins("http://localhost:3000");
                                registry.addMapping("/esquema/chase").allowedOrigins("http://localhost:3000");
			}
		};
	}

}
