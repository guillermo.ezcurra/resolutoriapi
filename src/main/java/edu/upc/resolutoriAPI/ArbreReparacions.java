/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.resolutoriAPI;

import edu.upc.mpi.chase.UtilsChase;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guille
 */
public class ArbreReparacions {
    private List<String> valor = null;
    private List<ArbreReparacions> children = new ArrayList<>();
    private List<String> pare = null;
    
    public ArbreReparacions(List<OrdinaryLiteral> dades) {
        valor = new ArrayList<>();
        for(OrdinaryLiteral ol : dades) {
            this.valor.add(ol.toString());
        }
    }
    public void afegirFill(ArbreReparacions fill) {
        this.children.add(fill);
        fill.afegirPare(this.valor);
    }
    public ArbreReparacions afegirFill(List<OrdinaryLiteral> dades) {
        ArbreReparacions nouFill = new ArbreReparacions(dades);
        children.add(nouFill);
        nouFill.afegirPare(this.valor);
        return nouFill;
    }
    public void afegirFills(List<ArbreReparacions> fills) {
        for(ArbreReparacions f : fills) {
            this.children.add(f);
            //f.afegirPare(this);
        }
    }
    public List<String> getName(){
        return this.valor;
    }
    public List<String> getPare(){
        return this.pare;
    }
    public List<ArbreReparacions> getChildren(){
        return this.children;
    }
    public List<ArbreReparacions> obteFills() {
        return children;
    }
    public List<String>  obteValor() {
        return valor;
    }
    public boolean esFulla() {
        return children.isEmpty();
    }
    public void setValor(List<OrdinaryLiteral>  dades) {
        this.valor = new ArrayList<>();
        for(OrdinaryLiteral ol : dades) {
            this.valor.add(ol.toString());
        }
    }
    public void setValorArrel(List<String>  dades) {
        this.valor = new ArrayList<>();
        for(String ol : dades) {
            this.valor.add(ol.toString());
        }
    }
    public void afegirValor(List<OrdinaryLiteral>  dades) {
        for(OrdinaryLiteral ol : dades) {
            if(!this.valor.contains(ol.toString())) {
                this.valor.add(ol.toString());
            }
        }
    }
    public void afegirPare(List<String> pare) {
        this.pare = pare;
    }
    
}
