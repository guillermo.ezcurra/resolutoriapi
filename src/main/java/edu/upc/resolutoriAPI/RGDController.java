/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.resolutoriAPI;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschemaRGD.EGD;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import edu.upc.mpi.logicschemaRGD.RGD;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author guille
 */
@RestController
public class RGDController {
    
    @PostMapping("/esquema/regles")
    public List<String> carregaRegles(@RequestParam(value="nomBD") String nomBD,@RequestParam(value="URL") String url,@RequestParam(value="username") String usuari, @RequestParam(value="clau") String clau,@RequestBody Map<String, Object> regles){
        List<String> reglesNoves = new ArrayList<>();
        for (Map.Entry<String,Object> entry : regles.entrySet()) {
            if(entry.getKey().equals("reglesNoves")) {
                reglesNoves = (List<String>) entry.getValue();
            }
        }
        String esquema = "";
        for(String rN : reglesNoves) {
           esquema += rN + "\n";
        }
        
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();   
        List<RGD> regles_rgd = resultLogicSchemaRGD.getAllRGDs();
        List<EGD> regles_egd = resultLogicSchemaRGD.getAllEGDs();
        List<NormalClause> regles_esquema = new LinkedList<>();
        regles_esquema.addAll(regles_rgd);
        regles_esquema.addAll(regles_egd);
        List<String> regles1 = new LinkedList<>();
        for(NormalClause r : regles_esquema) {
            regles1.add(r.toString());
        }
        return regles1;
    }
}
